﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

using System.Web.Mvc;
using MVC_API.Models;

namespace MVC.Controllers
{
    public class PersonasController : Controller
    {
        // GET: Personas
        public ActionResult Index()
        {
             List<persona> personas = null;
            using (var client = new HttpClient())   
            {
                    client.BaseAddress = new Uri("https://localhost:44380/api/");
                var response = client.GetAsync("personas");
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<persona>>();
                    readTask.Wait();
                    personas = readTask.Result;
                }
               else 
                {
                    personas = new List<persona> ();
                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
              }

            return View(personas);
        }

        // GET: Personas/Details/5
        public ActionResult Details(int id)
        {

            persona personas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44380/api/");
                var response = client.GetAsync("personas/"+id.ToString());
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<persona>();
                    readTask.Wait();
                    personas = readTask.Result;
                }
                else
                {
                    personas = new persona();
                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
            }

            return View(personas);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            return View();
           
        }

        // POST: Personas/Create
        [HttpPost]
        public ActionResult Create (persona personas )
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44380/api/");
                //http post
                var postTask = client.PostAsJsonAsync<persona>("personas", personas);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, "server error. Please contact admistrator");
            }
            return View(personas);
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int id)
        {


            persona personas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44380/api/");
                var response = client.GetAsync("personas/" + id.ToString());
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<persona>();
                    readTask.Wait();
                    personas = readTask.Result;
                }
                else
                {
                    personas = new persona();
                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
            }

            return View(personas);
        }

        // POST:     Personas/Edit/5
        [HttpPost]
        public ActionResult Edit( int id, persona personas)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44380/api/");
                //http post 
                var putTask = client.PutAsJsonAsync<persona>("personas/"+id.ToString(),personas);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }   
            return View(personas);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int id)
        {


            persona personas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44380/api/");
                var response = client.GetAsync("personas/" + id.ToString());
                response.Wait();

                var result = response.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<persona>();
                    readTask.Wait();
                    personas = readTask.Result;
                }
                else
                {
                    personas = new persona();
                    ModelState.AddModelError(string.Empty, "server error. Please contact administrador.");
                }
            }

            return View(personas);
        }


        // POST: Personas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, persona personas)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44380/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("personas/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return View(personas);
        }
    }
}
